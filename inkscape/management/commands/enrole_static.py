#
# Copyright 2021, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Pushes the static files from the collected directory into being editable.
"""
import os
import sys

from django.conf import settings
from django.core.management import BaseCommand
from django.db.utils import DataError

from inkscape.models import LocalStatic

def get_type(path):
    if path.endswith('.css'):
        return 'css'
    if path.endswith('.js'):
        return 'js'
    if path.rsplit('.', 1)[-1] in ('svg', 'jpg', 'jpeg', 'png', 'ico', 'gif'):
        return 'img'
    return '?'

class Command(BaseCommand):
    help = __doc__

    def handle(self, urls=(), *args, **options):
        if not os.path.isdir(settings.STATIC_ROOT):
            return sys.stderr.write("\nStatic directory doesn't exist or is "
                "empty. Have you run collectstatic yet?\n\n")

        for path in self._dir(settings.STATIC_ROOT, label='Static'):
            try:
                defaults = {'file_type': get_type(path)}
                obj, created = LocalStatic.objects.update_or_create(static_file=path, defaults=defaults)
            except DataError:
                print(f"Failed: {path}")
                continue
            if created:
                print(f"Created: {path}")

    def _dir(self, root, old=False, label=''):
        for name, _, files in os.walk(root, topdown=False):
            for fname in files:
                path = os.path.join(name, fname)
                yield os.path.join('..', 'static', path.replace(root, '').lstrip('/'))
