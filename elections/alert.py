#
# Copyright 2017, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Election Alerts, this section is fairly important as elections MUST keep
everyone up to date about what stage the election is at and what they
must do next.
"""
from django.utils.translation import override as tr_override, ugettext_lazy as _
from django.core.mail.message import EmailMultiAlternatives
from django.conf import settings

from alerts.template_tools import render_template, render_directly

from .models import Candidate, EMAIL_NONE, EMAIL_ADMIN, EMAIL_TEAM, EMAIL_ALL

DEFAULT_LANG = getattr(settings, 'LANGUAGE_CODE', 'en')

def send_invite_email(user, instance, **context):
    """Send an invite email"""
    email = user.email
    subject = f"Stand for Election: {instance.election}"
    template = "elections/alert/email_candidate_invitation_alert.txt"
    lang = user.language or DEFAULT_LANG
    send_email(email, subject, template, lang=lang, instance=instance,
               user=user, **context)


def send_team_email(team, subject, template, instance, to, **context):
    """Sends a team email to everyone"""
    recipients = []

    if to == EMAIL_NONE:
        return
    if to == EMAIL_ADMIN:
        recipients = [(team.admin.email, None)]
    if to == EMAIL_TEAM:
        if team.email:
            recipients = [(team.email, None)]
        else:
            recipients = [(team.admin.email, None)]
            # Do not translate this text to the administrator
            subject = "ADMIN WARNING! No group email set for team '%s'" % team.name
    if to == EMAIL_ALL:
        # If ballots have been sent, then we notify balloted members
        if instance.ballots.count():
            recipients = instance.ballots.values_list('user__email', 'user__language')
        else:
            recipients = team.members.values_list('user__email', 'user__language')

    for email, lang in recipients:
        if not lang:
            lang = DEFAULT_LANG
        send_email(email, subject, template, lang=lang, instance=instance, **context)

def send_email(email, subject, template, lang=DEFAULT_LANG, **context):
    """Sends the right emailusing the template"""
    with tr_override(lang):
        context['site'] = settings.SITE_ROOT
        return EmailMultiAlternatives(
            to=[email],
            body=render_template(template, context),
            subject=render_directly(subject, context)\
                .strip().replace('\n', ' ').replace('\r', ' '),
        ).send(True)
