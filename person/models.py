#
# Copyright 2014, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Customise the user model"""

import os

from datetime import timedelta

from django.conf import settings
from django.db.models.signals import post_save
from django.db.models import (
    F, Q, Max, Model, Manager, TextField, CharField, URLField,
    DateTimeField, BooleanField, IntegerField, ForeignKey, SlugField,
    ImageField, SET_NULL, CASCADE, QuerySet
)
from django.utils.timezone import now
from django.dispatch import receiver

from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _, get_language
from django.core.validators import MaxLengthValidator
from django.contrib.sessions.models import Session
from django.contrib.auth import SESSION_KEY

from django.contrib.auth.models import Group, AbstractUser, UserManager, Permission
from alerts.models import AlertSubscription

from inkscape.fields import ResizedImageField, AutoOneToOneField

from .peeps import get_peep_for

null = dict(null=True, blank=True) # pylint: disable=invalid-name

ROLE_STYLES = (
    ('plat', 'Platinum'),
    ('winner', 'Gold'),
    ('grey', 'Silver'),
    ('orange', 'Bronze'),
    ('purple', 'Purple'),
    ('blue', 'Blue'),
    ('red', 'Red'),
    ('green', 'Green'),
)
ENROLES = (
    ('-', _('Unknown')),
    ('O', _('Open')),
    ('P', _('Peer Approval')),
    ('T', _('Admin Approval')),
    ('C', _('Closed')),
    ('S', _('Secret')),
    ('E', _('Elected')),
)

def linked_users_only(qset, *rels):
    """
    Limits a query to only include enough user information to link to the user.
    """
    only = []
    for rel in rels:
        #for field in ('first_name', 'last_name', 'username'):
        for field in ('photo', 'bio', 'gpg_key', 'last_seen', 'website'):
            only.append(rel + '__' + field)
    return qset.select_related(*rels).defer(*only)

class PersonManager(UserManager):
    """Overwrite the creation functions because we customise is_staff"""
    def get_queryset(self):
        """Defer the gpg_key field, as it's not required the vast mojority of the time"""
        return super(PersonManager, self).get_queryset().defer('gpg_key')

    def _create_user(self, username, email, password, **extra_fields):
        add_is_staff = extra_fields.pop('is_staff', False)
        user = super()._create_user(username, email, password, **extra_fields)
        if add_is_staff:
            user.user_permissions.add(Permission.objects.get(codename='is_staff'))
        return user

class User(AbstractUser):
    """
    Take over the django auth user and provide a list of new fields as a user account.
    """
    bio = TextField(_('Bio'), validators=[MaxLengthValidator(4096)], **null)
    bio_html = BooleanField(default=False)
    photo = ResizedImageField(_('Photograph (square)'), upload_to='photos',
                              max_width=190, max_height=190, **null)
    language = CharField(_('Default Language'), max_length=8, choices=settings.LANGUAGES, **null)

    ircnick = CharField(_('IRC Nickname'), max_length=20, **null)
    ircpass = CharField(_('Freenode Password (optional)'), max_length=128, **null)

    website = URLField(_('Website or Blog'), **null)
    gpg_key = TextField(_('GPG Public Key'), validators=[MaxLengthValidator(262144)],\
        help_text=_('<strong>Signing and Checksums for Uploads</strong><br/> '
                    'Either fill in a valid GPG key, so you can sign your uploads, '
                    'or just enter any text to activate the upload validation feature '
                    'which verifies your uploads by comparing checksums.<br/>'
                    '<strong>Usage in file upload/editing form:</strong><br/>'
                    'If you have submitted a GPG key, you can upload a *.sig file, '
                    'and your upload can be verified. You can also submit these '
                    'checksum file types:<br/>'
                    '*.md5, *.sha1, *.sha224, *.sha256, *.sha384 or *.sha512'), **null)

    last_seen = DateTimeField(**null)
    visits = IntegerField(default=0)

    # Replaces is_staff from the parent abstractuser
    is_admin = BooleanField(_('staff status'), default=False, db_column='is_staff',\
        help_text=_('Designates whether the user can log into this admin site.'))

    objects = PersonManager()

    class Meta:
        permissions = [
            ("use_irc", _("IRC Chat Training Complete")),
            ("website_cla_agreed", _("Agree to Website License")),
            ("is_staff", "Staff permissions are automatically granted."),
        ]
        db_table = 'auth_user'

    def __str__(self):
        return self.name

    @property
    def is_staff(self):
        """
        Returns true if the user is a staff member (can access admin)
        takes over from the django is_staff field which we replace with
        a permission to allow groups of people to be staff.
        """
        return self.is_admin or self.has_perm('person.is_staff')

    def is_moderator(self):
        """Returns true if the user is a moderator"""
        return self.has_perm("moderation.can_moderate")

    def is_banned(self):
        """This user is banned"""
        return self.forum_flags.banned().count()

    def set_moderator(self, to=False):
        """Set moderator status"""
        if to:
            self.user_permissions.add(self._permission())
        else:
            self.user_permissions.remove(self._permission())

    def principle_team(self):
        """Return this user's principle team membership (if any)"""
        return self.memberships.filter(is_principle=True).first()

    def has_moderator(self):
        """Returns true only if this user has a moderator priv directly"""
        return self.user_permissions.filter(
            content_type__model='flagvote',
            codename='can_moderate').exists()

    def _permission(self, model='flagvote', codename='can_moderate'):
        """Get the moderator's permission (or a different permission)"""
        permissions = Permission.objects.filter(content_type__model=model)
        return permissions.get(codename=codename)

    @property
    def name(self):
        """Return either the full name of the user or the username"""
        if self.first_name or self.last_name:
            return self.get_full_name()
        return self.username

    def get_ircnick(self):
        """Return the irc nickname or the username (default)"""
        if not self.ircnick:
            return self.username
        return self.ircnick

    def photo_url(self):
        """Return the photo url if it exist"""
        if self.photo:
            return self.photo.url
        return get_peep_for(self.username)

    def photo_preview(self):
        """Returns a photo preview or a default svg file"""
        if self.photo:
            return '<img src="%s" style="max-width: 200px; max-height: 250px;"/>' % self.photo.url
        # Return an embedded svg, it's easier than dealing with static files.
        return """
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="200" height="250">
           <path style="stroke:#6c6c6c;stroke-width:.5px;fill:#ece8e6;"
           d="m1.2 1.2v248h27.6c-9.1-43 8-102 40.9-123-49.5-101 111-99.9 61.5 1.18 36.6 35.4 48.6 78.1 39.1 122h28.5v-248z"
           /></svg>"""
    photo_preview.allow_tags = True

    def quota(self):
        """Returns the amount of quota (disk space) this use has in total (not remaining)"""
        from resources.models import Quota
        groups = Q(group__in=self.groups.all()) | Q(group__isnull=True)
        quotas = Quota.objects.filter(groups)
        if quotas.count():
            return quotas.aggregate(Max('size'))['size__max'] * 1024
        return 0

    def get_absolute_url(self):
        """Return the url to the user's profile page"""
        if not self.username:
            return '/'
        return reverse('view_profile', kwargs={'username':self.username})

    def visited_by(self, by_user):
        """Adds one to the user's visitor count"""
        if by_user != self:
            self.visits += 1
            self.save(update_fields=['visits'])

    @property
    def teams(self):
        """Returns a queryset of teams this user is a member of"""
        return Team.objects.filter(group__in=self.groups.all())

    def forum_subscriptions(self):
        """Return a list of forum subscriptions"""
        from forums.alert import ForumTopicAlert
        return ForumTopicAlert.messages_for(self)

    def forum_topics(self):
        """Return a list of forum topics created"""
        from forums.models import ForumTopic
        return ForumTopic.objects.filter(first_username=self.username)

    def viewer_is_subscribed(self, user):
        """Returns true if the calling user is subscribed to this user's resources."""
        if user.is_authenticated:
            try:
                return bool(self.resources.subscriptions().get(user=user.pk))
            except AlertSubscription.DoesNotExist:
                return False
        return False


class SocialMediaSite(Model):
    """Social Media Website with user url template"""
    name = CharField(max_length=32)
    url_template = CharField(max_length=255,
        help_text="Add {id} where you need to specify the user's unique id.")
    icon = ImageField(upload_to='social_media', **null)
    default_site = CharField(max_length=48, **null,
        help_text="Default site, used for templating federated services.")
    is_visible = BooleanField(default=True)
    is_funding = BooleanField(default=False)

    def get_url(self, user_id):
        """Return the url for this chat channel"""
        site_id = self.default_site or ''
        if '@' in user_id[1:]:
            if user_id.startswith('https://'):
                (site_id, user_id) = user_id[8:].split('/', 1)
                user_id = user_id.split('@')[-1]
            elif user_id.count('@') == 2:
                (user_id, site_id) = user_id[1:].split('@', 1)

        try:
            return self.url_template.format(id=user_id, site=site_id)
        except (KeyError, AttributeError, ValueError) as err:
            return str(err)

    def __str__(self):
        return self.name

class UserSocialMedia(Model):
    """Link users to social media sites"""
    site = ForeignKey(SocialMediaSite, on_delete=SET_NULL,
                      related_name='users', null=True)
    user = ForeignKey(User, on_delete=CASCADE, related_name='social_media_urls')
    socid = CharField(_("Username on Website"), max_length=128)

    url = property(lambda self: self.site.get_url(self.socid))
    name = property(lambda self: self.site.name)
    is_visible = property(lambda self: self.site.is_visible)

    @property
    def icon(self):
        if self.site.icon:
            return self.site.icon.url
        return None

    class Meta:
        unique_together = ('site', 'user')


@receiver(post_save, sender=User)
def is_active_check(sender, instance, **_):
    """Delete every session when active is False"""
    if not instance.is_active:
        for session in Session.objects.all():
            # There is google-oauth sessions which aren't cleared here
            try:
                if int(session.get_decoded().get(SESSION_KEY, -1)) == instance.pk:
                    session.delete()
            except sender.DoesNotExist:
                pass

def group_breadcrumb_name(self):
    """Return the name of the group for breadcrumbs"""
    try:
        return str(self.team)
    except UnicodeDecodeError:
        return str(self)
Group.breadcrumb_name = group_breadcrumb_name


class TwilightSparkle(Manager):
    """The queryset manager for frienships"""
    def i_added(self, user):
        """Return true if I have added this friend before"""
        try:
            return user.is_authenticated and bool(self.get(from_user=user.pk))
        except Friendship.DoesNotExist:
            return False

    def mutual(self):
        """Returns a mutual set of friends"""
        return self.get_queryset()\
                .filter(from_user__from_friends__from_user=F('user'))

class Friendship(Model):
    """A single instance of friendship"""
    from_user = ForeignKey(User, related_name='friends', on_delete=CASCADE)
    user = ForeignKey(User, related_name='from_friends', on_delete=CASCADE)

    objects = TwilightSparkle()

    def __str__(self):
        return u"%s is friends with %s" % (str(self.from_user), str(self.user))

class TeamChatRoomType(Model):
    """A link to a chat room system"""
    name = CharField(max_length=255)
    url_template = CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Chat Room"
        verbose_name_plural = "Chat Rooms"

    def get_url(self, chat):
        """Return the url for this chat channel"""
        try:
            return self.url_template.format(
                team=chat.team,
                channel=chat.channel,
                language=chat.language)
        except (KeyError, AttributeError, ValueError) as err:
            return str(err)

    def __str__(self):
        return self.name

class TeamChatRoom(Model):
    """An ChatRoom for a team"""
    room_type = ForeignKey(TeamChatRoomType, blank=True, null=True,
        related_name='rooms', on_delete=CASCADE,
        help_text="The chat url template to use, if none uses IRC as the default.")
    team = ForeignKey('Team', related_name='chatrooms', on_delete=CASCADE)
    admin = ForeignKey(User, on_delete=SET_NULL, **null)
    channel = CharField(_('Chatroom Name'), max_length=64)
    language = CharField(max_length=5, default='en', choices=settings.LANGUAGES)

    class Meta:
        unique_together = (('language', 'room_type', 'team'),)

    def __str__(self):
        room_type = self.room_type or 'IRC'
        return f'{room_type}: {self.channel}'

    def get_absolute_url(self):
        if self.room_type:
            return self.room_type.get_url(self)
        return reverse("team.chat", kwargs={'team': self.team.slug})


class MembershipRole(Model):
    """
    Types of memberships which can be preset in a team, these presets allow the role
    to be easily moved between people. And selected by a user.
    """
    code = SlugField("Role URL Code", max_length=12, primary_key=True,\
        help_text="The simple lowercase 'slug' that is used in urls "
                  "(unique for whole website).")
    title = CharField(_("Role Title"), max_length=128)
    style = CharField(_("Role Style"), max_length=64, choices=ROLE_STYLES, **null)
    number = IntegerField(_("Maximum number"), default=0,\
        help_text="If greater than zero, will limit the number of this role to this count.")
    public = BooleanField(_("Publically Choosable"), default=True,\
        help_text="If set to false, this role can only be selected by team admins.")

    team = ForeignKey("Team", related_name='mtypes', on_delete=CASCADE)

    def __str__(self):
        return self.title

class TeamApproval(Model):
    """
    Contains a record of an admin, or peer, or own user adding/approving
    into a team membership. This allows us to record each time a user is added
    and by whom.
    """
    added = DateTimeField(auto_now_add=True)
    expired = DateTimeField(null=True, blank=True)
    team = ForeignKey('Team', related_name='membership_approvals', on_delete=CASCADE)
    enrole = CharField(_('Enrollment'), max_length=1, default='-', choices=ENROLES,
        help_text="The enrolement method when this user joined the team.")
    membership = ForeignKey('TeamMembership', related_name='approvals', on_delete=CASCADE, **null)
    reason = CharField(max_length=255,
        help_text="A brief note of why you approve or disapprove of this membership.", **null)

    target_user = ForeignKey(User, related_name='membership_approvals', on_delete=SET_NULL, **null,
        help_text="The user who's team membership is being modified.")
    acting_user = ForeignKey(User, related_name='membership_actions', on_delete=SET_NULL, **null,
        help_text="The user who is approving this change.")

    APPROVAL_ACTIONS = (
        (True, "Approve user"),
        (False, "Disapprove user"),
    )
    approves = BooleanField(_('Team Action'), default=True, choices=APPROVAL_ACTIONS)

    class Meta:
        unique_together = ('team', 'target_user', 'acting_user')

    def __str__(self):
        target = self.target_user
        if target == self.acting_user:
            target = _("themselves")
        if self.approves:
            return str(_(f"{self.acting_user} approves of {target} being a member of {self.team}"))
        return str(_(f"{self.acting_user} disapproves of {target} being a member of {self.team}"))

    def get_invitation_url(self):
        """An invitation's response is the join button"""
        return reverse('team.join', kwargs={'team': self.team.slug})

    def save(self, *args, **kwargs):
        if not self.enrole or self.enrole == '-' and self.team:
            self.enrole = self.team.enrole
        return super().save(*args, **kwargs)

class TeamMembership(Model):
    """
    A team membership exists when a user joins a team, existance doesn't mean
    they are a member of that team, just that they were, are or will be one.

    It functions as a record of when a user joined a team, when they should
    expire from the team and what date time they did expire.

    Joining a team sets the 'joined' datetime, which tags this user as
    a current member. It will also unset the 'expired' field.

    Leaving a team (manually or automatically) sets the 'expired' datetime
    which tags this user as being a 'past' member.
    """
    team = ForeignKey('Team', related_name='memberships', on_delete=CASCADE)
    user = ForeignKey(User, related_name='memberships', on_delete=CASCADE)

    requested = DateTimeField(**null)
    joined = DateTimeField(**null)
    expired = DateTimeField(**null)

    added_by = ForeignKey(User, related_name='has_added_users', on_delete=SET_NULL, **null)
    removed_by = ForeignKey(User, related_name='has_removed_users', on_delete=SET_NULL, **null)

    title = CharField(_('Custom Role Title'), max_length=128, **null)
    style = CharField(_('Custom Role Style'), max_length=64, choices=ROLE_STYLES, **null)
    role = ForeignKey(MembershipRole, related_name="memberships", on_delete=CASCADE, **null)
    is_principle = BooleanField(_('Principal Team Membership'), default=False)
    request_text = CharField(_('Request to Join'), max_length=255, **null)

    parent = property(lambda self: self.team)

    def __str__(self):
        return 'Membership'

    @property
    def is_watcher(self):
        """Return true if this user is watching this team"""
        return not self.requested and not self.joined and not self.is_expired

    @property
    def is_requester(self):
        """Return true if this user is requesting membership to team"""
        return bool(self.requested) and not self.joined and not self.is_expired

    @property
    def is_member(self):
        """Return true if this user is a member of this team"""
        return self.joined and not self.is_expired

    @property
    def is_expired(self):
        """Return true if this user is expired"""
        return not (self.expired is None or self.expired > timezone.now())

    def all_approvals(self):
        """Return a list of approvals"""
        return self.team.approvals(self.user)

    def get_invitation(self):
        """Returns the TeamApproval this user was invited with, or None if they requested membership"""
        approval = self.all_approvals().order_by('added').first()
        if approval and self.requested and self.requested > approval.added:
            return approval
        return None

    class Meta:
        unique_together = ('team', 'user')

    def save(self, **kwargs): # pylint: disable=arguments-differ
        """Control the group of users (which grants permissions)"""
        super(TeamMembership, self).save(**kwargs)
        if self.id:
            self.update_group()

    def update_group(self):
        """
        Makes sure the user is registered in the right group. Keeping
        the team membership and the group permissions sync'ed.
        """
        user_group = self.team.group.user_set
        if not self.is_expired and self.joined:
            user_group.add(self.user)
        else:
            user_group.remove(self.user)

    def get_absolute_url(self, for_user=None):
        """Return link to membership, either by membership or for an admin"""
        if for_user == self.user:
            return reverse('team.membership', kwargs={'team': self.team.slug})
        return reverse('team.membership', kwargs={'team': self.team.slug, 'pk': self.pk})

class BlacklistManager(QuerySet):
    def blacklists(self, email):
        return EmailBlacklist.objects.filter(server__in=list(self._server_chunks(email.split('@')[-1])))

    @staticmethod
    def _server_chunks(domain):
        if '.' in domain:
            yield domain
            yield from BlacklistManager._server_chunks(domain.split('.', 1)[-1])

class EmailBlacklist(Model):
    """A list of email servers to ban from registration"""
    server = CharField(max_length=128, primary_key=True)
    reason = CharField(max_length=255)

    objects = BlacklistManager.as_manager()

    def __str__(self):
        return self.server

class Team(Model):
    """
    A team is a shadow object for a group, it doesn't record a list of users that
    is done by django.auth.Group, but it does control everything else about memberships.
    """
    ICON = os.path.join(settings.STATIC_URL, 'images', 'team.svg')

    admin = ForeignKey(User, related_name='admin_teams', on_delete=SET_NULL, **null)
    group = AutoOneToOneField(Group, related_name='team', on_delete=CASCADE)
    email = CharField(_('Email Address'), max_length=256, **null)

    name = CharField(_('Team Name'), max_length=32)
    slug = SlugField(_('Team URL Slug'), max_length=32)
    icon = ImageField(_('Display Icon'), upload_to='teams', default=ICON)
    badge = ImageField(_('User Badge'), upload_to='teams', **null)

    order = IntegerField(default=0)
    intro = TextField(_('Introduction'), validators=[MaxLengthValidator(1024)],\
        help_text=_("Text inside the team introduction."), **null)
    desc = TextField(_('Full Description'), validators=[MaxLengthValidator(10240)],\
        help_text=_("HTML description on the teams front page."), **null)
    charter = TextField(_('Charter'), validators=[MaxLengthValidator(30240)],\
        help_text=_("HTML page with rules for team members."), **null)
    side_bar = TextField(_('Side Bar'), validators=[MaxLengthValidator(10240)],\
        help_text=_("Extra sie bar for buttons and useful links."), **null)

    mailman = CharField(_('Email List'), max_length=32, null=True, blank=True,\
        help_text='The name of the pre-configured mailing list for this team')
    enrole = CharField(_('Enrollment'), max_length=1, default='O', choices=ENROLES)
    enrole_msg = TextField(_('Enrolement Message'), null=True, blank=True,\
        help_text='This message is displayed to users requesting to join the team.')
    enrole_peers = IntegerField(default=1,
        help_text='The number of peers needs to approve this user when enrole is set to PEER APPROVAL')

    auto_expire = IntegerField(default=0,\
        help_text=_('Number of days that members are allowed to be a member.'))

    localized_fields = ('name', 'intro', 'desc', 'charter', 'side_bar')

    class Meta:
        ordering = ('order',)

    @property
    def irc_channels(self):
        """Return a list of all chat channels"""
        return self.chatrooms.filter(room_type__isnull=True, language__in=[get_language(), 'en'])\
                             .values('language', 'channel')

    @property
    def team(self):
        """Return self as the team, used in mixology"""
        return self

    def list_email(self):
        if self.mailman and '/' not in self.mailman:
            return f'{self.mailman}@lists.inkscape.org'
        return None

    parent = property(lambda self: (reverse('teams'), _('Teams')))

    @property
    def peers(self):
        """Return a list (not queryset) or members who can approve others"""
        if self.enrole == 'P':
            return [member.user for member in self.members] + [self.admin]
        return [self.admin]

    def get_absolute_url(self):
        """Return link to team page"""
        return reverse('team', kwargs={'team': self.slug})

    def save(self, **kwargs): # pylint: disable=arguments-differ
        if not self.name:
            self.name = self.group.name
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Team, self).save(**kwargs)

    def get_members(self, joined=True, expired=False, requested=None):
        """
        Returns a QuerySet containing the given memberships as they related to
        this team. See convience functions below.

        Default is to return all joined (real) members of a team.
        """
        qset = Q()
        if expired is True:
            qset &= Q(expired__lt=timezone.now())
        elif expired is False:
            qset &= (Q(expired__isnull=True) | Q(expired__gt=timezone.now()))
        if requested is not None:
            qset &= Q(requested__isnull=not requested)
        if joined is not None:
            qset &= Q(joined__isnull=not joined)
        return self.memberships.filter(qset)

    requests = property(lambda self: self.get_members(False, False, True).order_by('-requested'))
    watchers = property(lambda self: self.get_members(False, False, False))
    members = property(lambda self: self.get_members(True).order_by('joined'))

    old_requests = property(lambda self: self.get_members(False, True, True).order_by('-expired'))
    old_watchers = property(lambda self: self.get_members(False, True, False))
    old_members = property(lambda self: self.get_members(True, True).order_by('-expired'))

    def has_member(self, user):
        """Returns true if the user is a member of the team"""
        return self.members.filter(user_id=user.id).count() == 1

    def get_membership(self, user):
        """Returns a valid active membership is available"""
        return self.members.filter(
            user_id=user.id,
            expired__isnull=True,
            joined__isnull=False).first()

    def has_requester(self, user):
        """Returns true if the user is requesting to join the team"""
        return self.requests.filter(user_id=user.id).count() == 1

    def has_watcher(self, user):
        """Returns true if the user is watching this team"""
        return self.watchers.filter(user_id=user.id).count() == 1

    def update_membership(self, user, **kw):
        """Generic update function for views to change membership"""
        return self.memberships.update_or_create(user=user, defaults=kw)

    def expire_if_needed(self, before_date=None):
        """Expire any members who have expired from this team"""
        if before_date is None:
            before_date = now()
        delta = timedelta(days=self.auto_expire)
        for membership in self.members:
            if membership.joined + delta < before_date:
                membership.approvals.update(expired=now())
                self.update_membership(membership.user, expired=now(), removed_by=None)
                yield membership.user

    def warn_if_needed(self, before_date=None, days=5):
        """Warn a user they are about to be expired from the team"""
        if before_date is None:
            before_date = now()
        delta = timedelta(days=self.auto_expire - days)
        for membership in self.members:
            if (membership.joined + delta).date == before_date.date:
                # XXX Send warning email
                yield membership.user

    def send_membership_email(self, obj):
        """Send a membership email about this request"""
        from .alert import RequestToJoinAlert
        RequestToJoinAlert.bellow(instance=obj)

    def send_invitation_email(self, obj):
        from .alert import InviteToJoinAlert
        InviteToJoinAlert.bellow(instance=obj)

    def send_report_email(self, obj):
        from .alert import MembershipAdded
        MembershipAdded.bellow(instance=obj)

    def approvals(self, user, **kwargs):
        return TeamApproval.objects.filter(team=self, target_user=user, expired__isnull=True, **kwargs)

    def _is_approved(self, user):
        """Assuming this user is authorised"""
        if self.enrole == 'P':
            return self.approvals(user, approves=True).count() >= self.enrole_peers
        return True # Automatic yes

    def join(self, user, actor=None, reason=None):
        """
        Joins this team or requests membership

        Returns (membership, message)

        Message level is info if membership exists, and error is not.
        """
        if not user:
            return (None, _("Email address is not found."))
        if not user.email:
            return (None, _("User must have an email address set in their profile to get membership to a team."))
        if reason and len(reason) > 254:
            return (None, _("Reason can only be 255 letters long, please shorten your request message."))

        if self.enrole == 'O' or \
          (actor == self.admin and self.enrole in 'PT') or \
          (self.has_member(actor) and self.enrole == 'P' and actor != user):

            (approval, is_new_approval) = TeamApproval.objects.update_or_create(
                    team=self, target_user=user, acting_user=actor,
                    defaults={'reason': reason, 'approves': True})

            if not approval.approves:
                approval.approves = True
                approval.save()
            
            if user == actor or self.has_requester(user):
                if self.has_member(user):
                    return ('warning', _("You are already a member."))

                if not self._is_approved(user) and actor != self.admin:
                    if is_new_approval is True:
                        return ('success', _('Approval of membership saved.'))
                    elif is_new_approval is False:
                        return ('info', _('Approval of membership already saved.'))

                (obj, created) = self.update_membership(
                    user, expired=None, joined=now(), added_by=actor)
                self.send_report_email(obj)

                # Backlink the approval to the membership
                self.approvals(user, membership__isnull=True).update(membership=obj)
                return (obj, _("Team membership sucessfully added."))

            if is_new_approval:
                if self.approvals(user).count() == 1:
                    self.send_invitation_email(approval)
                    return ('success', _("Invitation to join this team sent to user."))
                else:
                    return ('info', _("Invitation to join this team already sent to user."))
            return ('warning', _("This user has not requested membership or replied to an invitation."))

        elif self.enrole in 'PT' and actor == user:
            (obj, created) = self.update_membership(user, requested=now())
            if self.enrole == 'P' and self._is_approved(user):
                (obj, created) = self.update_membership(user, expired=None,
                                                        joined=now(), added_by=user)
                self.send_report_email(obj)
                return (obj, _("Membership Invitation Accepted."))
            elif created or (obj.joined and obj.expired):
                self.update_membership(user, expired=None, joined=None, request_text=reason)
                self.send_membership_email(obj)
                return (obj, _("Membership Request Received."))
            elif obj.joined:
                return (obj, _("You are already a member of this team."))
            elif obj.expired:
                return (obj, _("Your membership was previously rejected, "\
                    "please contact the website administrator if this is in error."))
            return (obj, _("Already requested a membership to this team."))

            return obj.get_absolute_url(for_user=user)
        return (None, _("Can't add user to team. (not allowed)"))

    def __str__(self):
        return self.name


def get_team_url(self):
    """Patch in the url so we get a better front end view from the admin."""
    try:
        return self.team.get_absolute_url()
    except Team.DoesNotExist:
        return '/'
Group.get_absolute_url = get_team_url
