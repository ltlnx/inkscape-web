# -*- coding: utf-8 -*-
#
# Copyright 2014-2017, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Customise the user authentication model"""


from django.views.generic import (
    UpdateView, DetailView, ListView, RedirectView, TemplateView, FormView, View
)
from django.template.response import TemplateResponse
from django.views.generic.detail import SingleObjectMixin
from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.timezone import now
from django.core.mail import send_mail
from django.contrib import messages
from django.contrib.auth.models import Permission
from django.contrib.auth.views import LoginView as BaseLoginView
from django.contrib.messages import get_messages
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.urls import translate_url

from django_registration.backends.activation.views import RegistrationView

from .models import User, Team, TeamMembership, TeamApproval
from .forms import UserForm, AgreeToClaForm
from .team_forms import TeamEdit, TeamMessageForm
from .mixins import UserRequired, NeverCacheMixin, UserMixin, NextUrlMixin, TeamMixin

class LoginView(BaseLoginView):
    def get_success_url(self):
        url = super().get_success_url()
        lang = self.request.user.language or get_language()
        return translate_url(url, lang)

class AgreeToCla(NeverCacheMixin, NextUrlMixin, UserMixin, UpdateView):
    template_name = 'person/cla-agree.html'
    title = _('Contributors License Agreement')
    form_class = AgreeToClaForm

class EmailChangedView(RegistrationView):
    email_body_template = 'django_registration/reactivation_email_body.txt'
    email_subject_template = 'django_registration/reactivation_email_subject.txt'


class EditProfile(NeverCacheMixin, NextUrlMixin, UserMixin, UpdateView):
    title = _('Edit User Profile')
    form_class = UserForm

    def get_success_url(self):
        return self.object.get_absolute_url()

    def form_valid(self, form):
        new_email = form.cleaned_data.get('email')
        old_email = User.objects.get(pk=self.object.pk).email
        # Save data before checking email
        ret = super().form_valid(form)
        if ret and new_email != old_email:
            self.email_changed(form.instance, old_email, new_email)
        return ret

    def email_changed(self, instance, old_email, new_email):
        """
        The email address was changed, so reset the active flag and email them.
        """
        # We update the last_login on email change to prevent change
        # password attacks which would allow an attacker re-access to
        # an account. Changing the date here will invalidate any hash.
        #instance.last_login = now()
        self.request.session.flush()
        # Deactivate account and send new confirmation email
        instance.is_active = False
        instance.save()

        # send email activation
        fake_registration = EmailChangedView(request=self.request)
        fake_registration.send_activation_email(instance)

        messages.warning(self.request,\
            _("Your email address has changed, we have sent a new confirmation email. "
              "Please confirm your new email address to log back in."))

        # Send notification email to old email address just in case it's not authorised.
        send_mail(
            _("Email Address Changed"),
            _("Your email address on inkscape.org has changed from %(old)s "
              "to %(new)s, if this is incorrect, please email the webmaster "
              "for assistance."
            ) % {'old': old_email, 'new': new_email},
            "Inkscape Webmaster <webmaster@inkscape.org>",
            [old_email])


class UserDetail(DetailView):
    template_name  = 'person/user_detail.html'
    slug_url_kwarg = 'username'
    slug_field     = 'username'
    model = User

    def get_object(self, **kwargs):
        user = super(UserDetail, self).get_object(**kwargs)
        user.visited_by(self.request.user)
        return user

class UserGPGKey(UserDetail):
    template_name = 'person/gpgkey.txt'
    content_type = "text/plain"

class MyProfile(NeverCacheMixin, UserMixin, UserDetail):
    pass
  
class Welcome(UserMixin, TemplateView):
    template_name = 'django_registration/welcome.html'
    title = _('Welcome')

# ====== FRIENDSHIP VIEWS =========== #

class MakeFriend(NeverCacheMixin, UserRequired, SingleObjectMixin, RedirectView):
    slug_url_kwarg = 'username'
    slug_field     = 'username'
    model          = User
    permanent      = False

    def get_object(self):
        user = SingleObjectMixin.get_object(self)
        (obj, new) = self.request.user.friends.get_or_create(user=user)
        if new:
            messages.success(self.request, "Friendship created with %s" % str(user))
        else:
            messages.error(self.request, "Already a friend with %s" % str(user))
        return user

    def get_redirect_url(self, **kwargs):
        return self.get_object().get_absolute_url()

class LeaveFriend(MakeFriend):
    def get_object(self):
        user = SingleObjectMixin.get_object(self)
        self.request.user.friends.filter(user=user).delete()
        messages.success(self.request, "Friendship removed from %s" % str(user))
        return user


# ====== TEAM VIEWS ====== #

class TeamList(ListView):
    queryset = Team.objects.exclude(enrole='S')
    title = _('Teams')

class TeamDetail(TeamMixin, DetailView):
    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            data['membership'] = data['object'].get_membership(user)
            data['is_peer'] = data['object'].admin == user
            if data['object'].enrole == 'P':
                data['is_peer'] = data['is_peer'] or data['membership']
        data['members'] = data['object'].members.order_by('-user__last_seen')
        return data

class TeamCharter(TeamDetail):
    title = _("Team Charter")
    template_name = 'person/team_charter.html'

class TeamMemberList(TeamDetail):
    template_name = 'person/team_memberlist.txt'
    content_type = "text/plain; charset=utf-8"

class EditTeam(UserRequired, TeamMixin, UpdateView):
    form_class = TeamEdit

    def is_allowed(self, user):
        return user == self.get_object().admin


class ChatWithTeam(NeverCacheMixin, UserRequired, TeamDetail):
    title = _("Chat")

    @property
    def template_name(self):
        if not self.request.user.has_perm('person.use_irc') \
          or 'tutorial' in self.request.GET:
            return 'chat/tutorial.html'
        return 'person/team_chat.html'

    def get(self, request, *args, **kwargs):
        if request.GET.get('irc') == 'ok':
            irc = Permission.objects.get(codename='use_irc')
            request.user.user_permissions.add(irc)
            return HttpResponseRedirect(request.path)
        return super(ChatWithTeam, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(ChatWithTeam, self).get_context_data(**kwargs)
        data['object'] = self.get_object()
        data['page'] = int(self.request.GET.get('page', 1))
        data['chat_page'] = "chat/page-%s.svg" % data['page']
        return data


class MembershipRequestView(NeverCacheMixin, UserRequired, DetailView):
    """
    View a membership request, with the ability to comment on them.
    """
    model = TeamMembership
    def get_title(self):
        obj = self.get_object()
        if obj.is_member:
            return _("%(team)s for %(user)s") % {'team': obj.team, 'user': obj.user}
        return _("Request to join %s") % (obj.team)

    def is_allowed(self, user):
        """Can the user access this request view"""
        obj = self.get_object()
        return obj.user == user \
            or obj.team.admin == user \
            or (obj.team.enrole == 'P' and obj.team.has_member(user)) \
            or user.is_superuser

    def get_object(self, queryset=None):
        """The object is based on the user currently logged in"""
        if 'pk' not in self.kwargs:
            team = Team.objects.get(slug=self.kwargs['team'])
            try:
                return team.memberships.get(user=self.request.user)
            except TeamMembership.DoesNotExist:
                raise Http404()
        # Primary Key lookup is used for admins to see requests
        return super().get_object(queryset=queryset)

class AddMember(NeverCacheMixin, UserRequired, SingleObjectMixin, RedirectView):
    """
    Add a user to a team, this membership may not be complete after this action and
    may need peer or administration approval.
    """
    slug_url_kwarg = 'team'
    permanent = False
    model = Team

    def get_user(self):
        username = self.kwargs.get('username', None)

        # From invitations popUp
        if not username and 'invite' in self.request.POST:
            username = self.request.POST['invite']
            if self.reason is None:
                self.reason = "Inviting User"
            if '@' in username:
                try:
                    return User.objects.get(email=username)
                except User.MultipleObjectsReturned:
                    messages.warning(self.request, "Multiple users have this email address (which is odd)")
                    return User.objects.filter(email=username).first()
                except User.DoesNotExist:
                    return self.invite_anonymouse(username)

        if username:
            return User.objects.get(username=username)
        return self.request.user

    def is_allowed(self, user):
        # Already invited to the team by someone else, approved!
        if self.get_object().membership_approvals.filter(target_user=user).count():
            return True
        # Limit joining to people who can post to the forums
        # XXX This needs to be rethought as it's too restrictive.
        #if not user.has_perm('forums.can_post_comment'):
        #    messages.warning(self.request, _("Joining a team, or inviting others, requires vetting. Please post to the forums first to ask for the required permissions."))
        #    return False
        return True

    def invite_anonymouse(self, email):
        # TODO: In the future we could send an email here, but it would be tricky for spam.
        messages.error(self.request, f"Can not invite user '{email}' directly, please ask them to join the website first.")
        return None

    def action(self, user, actor=None):
        return self.get_object().join(user, actor=actor, reason=self.reason)

    def get_redirect_url(self, **kwargs):
        self.reason = self.request.POST.get('note', None)
        try:
            next_url = self.get_object().get_absolute_url()
            user = self.get_user()
            if not user:
                return self.request.GET.get('next', next_url)
            (membership, msg) = self.action(user, self.request.user)
            if not membership or isinstance(membership, str):
                getattr(messages, membership or 'error')(self.request, msg)
            elif msg:
                messages.info(self.request, msg)
            if isinstance(membership, TeamMembership):
                next_url = membership.get_absolute_url(for_user=user)
        except (Team.DoesNotExist, User.DoesNotExist) as err:
            raise Http404(str(err))
        return self.request.GET.get('next', next_url)

class RemoveMember(AddMember):
    """
    Remove member request, watching or membership itself.
    """
    def action(self, user, actor=None):
        """Action which is called by AddMember.get_redirect_url"""
        team = self.get_object()
        if actor in [user, team.admin]:
            if team.has_member(user):
                (membership, created) = team.update_membership(user, expired=now(), removed_by=actor)
                membership.approvals.update(expired=now())
                if actor == user:
                    return (membership, _("You have resigned from this team."))
                return (membership, _("User removed from team."))
            elif team.has_requester(user):
                (membership, created) = team.update_membership(user, expired=now(), removed_by=actor)
                if actor == user:
                    return (membership, _("Your membership requests has been removed."))
                return (membership, _("User removed from membership requests."))
            elif team.has_watcher(user):
                (membership, created) = team.update_membership(user, expired=now(), removed_by=actor)
                if actor == user:
                    return (membership, _("You have stopped watching this team."))
                return (membership, _("User removed from watching team."))
            return (None, _("Cannot remove user from team. (not allowed)"))
        # If this user is still in request mode, the user may be removing their approval
        if team.has_requester(user):
            (approval, is_new_approval) = TeamApproval.objects.update_or_create(
                team=team, target_user=user, acting_user=actor,
                defaults={'reason': self.reason, 'approves': False})

            if is_new_approval:
                return ('success', _("Disapproval added."))
            else:
                return ('info', _("Disapproval updated."))

        return (None, _("Cannot remove user from team. (not allowed)"))

class WatchTeam(AddMember):
    """
    Add a membership which is only concerned with watching the team.
    """
    def action(self, user, actor=None):
        """Action which is called by AddMember.get_redirect_url"""
        team = self.get_object()
        if team.enrole == 'S':
            return (None, _("You can't watch this team."))

        membership, created = team.update_membership(user, expired=None, requested=None, joined=None)
        return (membership, _("Now watching this team."))

class UnwatchTeam(AddMember):
    """
    Remove a watching membership specifically.
    """
    def action(self, user, actor=None):
        """Action which is called by AddMember.get_redirect_url"""
        team = self.get_object()
        membership, created = team.update_membership(user, expired=now(), removed_by=actor)
        return (membership, _("No longer watching this team."))

class UserAvatar(View):
    """
    Attempts to find a user's avatar by their IRC nickname or website username.
    """
    permanent = True
    skip_middleware = True

    def get_object(self, nick):
        """Get the nearest matching user with this nickname"""
        user = User.objects.filter(ircnick=nick).order_by('pk').first()
        if user is None:
            user = User.objects.filter(username=nick).order_by('pk').first()
        return user, nick

    def get(self, request, nick, ext=None):
        """Redirect to the given picture file."""
        from django.http import HttpResponse
        from resources.utils import FileEx

        user, nick = self.get_object(nick)
        if user is None or not user.photo:
            return self.generate_svg(nick)
        user.photo.open()
        fex = FileEx(user.photo.path)
        return HttpResponse(user.photo.read(), content_type=str(fex.mime))

    @staticmethod
    def string_to_colour(string):
        """Convert a string into a colour for use in a generated avatar"""
        total = 0

        for char in string:
            total = ord(char) + ((total << 5) - total)

        return '#' + ''.join(['{:02x}'.format((total >> (i * 8)) & 0xFF) for i in range(3)])

    def generate_svg(self, nick):
        """Return a generated svg based on a template"""
        return TemplateResponse(
            self.request,
            template='person/irc_avatar.svg',
            context={'nick': nick, 'colour': self.string_to_colour(nick)},
            content_type='image/svg+xml')

class ContactAllTeam(UserRequired, TeamMixin, SingleObjectMixin, FormView):
    title = _('Contact all Team Members')
    template_name = 'person/team_contact_all.html'
    form_class = TeamMessageForm

    def is_allowed(self, user):
        self.object = self.get_object()
        return user == self.object.admin

    def form_valid(self, form):
        from .alert import MessageToTeam
        count = MessageToTeam.bellow(instance=self.get_object(),
            subject=form.cleaned_data['subject'],
            body=form.cleaned_data['comment'])
        messages.info(self.request, _("Messages sent to {} member(s)").format(count))
        return super().form_valid(form) 

    def get_success_url(self):
        return self.get_object().get_absolute_url()

class GetMessages(View):
    """Returns a json of all available messages on the stack"""
    def get(self, request):
        return JsonResponse({'messages': [str(item) for item in get_messages(request)]})
