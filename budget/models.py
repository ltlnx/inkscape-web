#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Budgeting models
"""

from decimal import Decimal
from django.db.models import (
    Model, Manager, CASCADE, SET_NULL,
    ForeignKey, OneToOneField, IntegerField, DateTimeField, BooleanField,
    CharField, SlugField, TextField, PositiveIntegerField, DateField,
    DecimalField,
)
from django.core.validators import MaxLengthValidator

from django.conf import settings
from django.urls import reverse
from django.utils.timezone import now

from django.utils.translation import ugettext_lazy as _

from person.models import Team

CURRENCIES = (
    ('USD', 'Dollars (USD)'),
)

class Budget(Model):
    """A single budget"""
    team = ForeignKey(Team, null=True, blank=True, related_name='budgets', on_delete=SET_NULL)
    name = CharField(max_length=128, unique=True)
    slug = SlugField(max_length=128, unique=True)
    desc = TextField(validators=[MaxLengthValidator(4096)], null=True, blank=True)

    currency = CharField(max_length=3, default='USD', choices=CURRENCIES)

    date_display = CharField(max_length=128, default="%d",
        help_text="The format for displaying this budget format.")

    manager = ForeignKey(settings.AUTH_USER_MODEL,
        on_delete=SET_NULL, null=True, related_name='manager_for_budgets',
        help_text="The person responsible for keeping this record up to date.")

    def get_absolute_url(self):
        return reverse('budget:view', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name

class BudgetCategory(Model):
    """A category of the budget"""
    budget = ForeignKey(Budget, related_name='categories', on_delete=CASCADE)
    name = CharField(max_length=128, unique=True)
    sort = IntegerField(default=0)

    is_income = BooleanField(default=False)
    is_active = BooleanField(default=True)
    is_previous = BooleanField(default=False)

    class Meta:
        ordering = ('-sort',)

    def __str__(self):
        return self.name

class BudgetEntry(Model):
    """A single column"""
    budget = ForeignKey(Budget, related_name='entries', on_delete=CASCADE)
    point = DateField(default=now)
    total = DecimalField(default=None, decimal_places=2, max_digits=10, null=True)

    is_locked = BooleanField(default=False)

    created_by = ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=SET_NULL)
    created_on = DateTimeField(default=now)

    def __str__(self):
        return f"{self.point}"

    def get_total(self):
        """Calculate the total for this budget entry"""
        if self.total is None:
            # Currently entries are not linked to previous entries so no values
            # roll over to the next entry. A user must input previous totals as income.
            total = Decimal(0.0)
            for val in self.data.all():
                if val.category.is_income:
                    total += val.value
                else:
                    total -= val.value
            self.total = total
            self.save()
        return self.total

    def get_previous(self):
        """Returns the previous entry point, or None"""
        try:
            return self.get_previous_qset().latest()
        except BudgetEntry.DoesNotExist:
            return None

    def get_previous_qset(self):
        return BudgetEntry.objects.filter(point__lt=self.point)

    class Meta:
        unique_together = ('budget', 'point')
        get_latest_by = 'point'

    def save(self, *args, **kwargs):
        # Lock any previous entires
        self.get_previous_qset().update(is_locked=True)
        return super().save(*args, **kwargs)

class EntryValue(Model):
    """A single datum in the column"""
    entry = ForeignKey(BudgetEntry, related_name='data', on_delete=CASCADE)
    category = ForeignKey(BudgetCategory, related_name='values', on_delete=CASCADE)

    value = DecimalField(default=0.0, decimal_places=2, max_digits=10)

    def __str__(self):
        return f"{self.entry}: {self.category}"

    class Meta:
        ordering = ('entry__point', '-category__sort')
        unique_together = ('entry', 'category')

    def save(self, *args, **kwargs):
        # Reset total when value changes
        BudgetEntry.objects.filter(pk=self.entry_id).update(total=None)
        return super().save(*args, **kwargs)

