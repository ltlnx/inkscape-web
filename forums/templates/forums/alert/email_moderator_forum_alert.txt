{% load i18n inkscape %}{% autoescape off %}{% blocktrans with user=alert.user %}Dear {{ user }},{% endblocktrans %}
{% if action == 'edit' %}
{% blocktrans with user=comment.user %}A post has been edited on the forum "{{ instance }}" by {{ user }}:{% endblocktrans %}{% elif action == 'del' %}{% if mod %}
{% blocktrans with user=comment.user %}A post has been removed on the forum "{{ instance }}" by moderator {{ mod }}:{% endblocktrans %}{% else %}
{% blocktrans with user=comment.user %}A post has been removed on the forum "{{ instance }}":{% endblocktrans %}{% endif %}{% else %}
{% blocktrans with user=comment.user %}A new post on the forum "{{ instance }}" by {{ user }}:{% endblocktrans %}{% endif %}
---
{{ comment.comment|striptags|decodetext }}
----
{% trans "Link:" %} {{ site }}{{ comment.get_topic.get_absolute_url }}#c{{ comment.pk }}{% endautoescape %}
