# -*- coding: utf-8 -*-
#
# Copyright 2013, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=too-many-ancestors
"""
Views for resource system, adding items, entering new categories for widgets etc
"""

from django.urls import reverse
from django.http import JsonResponse, Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import Group
from django.contrib import messages
from django.views.generic import DetailView, ListView, DeleteView, CreateView, UpdateView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.base import RedirectView
from django.template.defaultfilters import filesizeformat
from django.db.models import F, Q, Count, OuterRef, Subquery

from person.models import User, Team
from releases.forms import ResourceReleaseForm

from .utils import RemoteError
from .video_url import parse_any_url
from .category_views import CategoryListView
from .mixins import (
    OwnerDeleteMixin, OwnerCreateMixin, OwnerUpdateMixin,
    OwnerViewMixin, ResourceJSONEncoder, GalleryMixin
)
from .rss import ListFeed, ExternalMixin
from .models import Category, License, Gallery, Resource, Tag, Vote
from .forms import (
    GalleryForm, GalleryMoveForm, AddLinkForm, RenameResourceForm,
    ResourceForm, ResourceBaseForm, ResourceAddForm, ResourcePasteForm,
    ResourceCodeForm,
)
from .voting import gallery_count, gallery_stv


class DeleteGallery(GalleryMixin, OwnerDeleteMixin, DeleteView):
    """An owner of a gallery can delete it"""
    title = _("Delete Gallery")

class CreateGallery(GalleryMixin, OwnerCreateMixin, CreateView):
    """Any user can create galleries"""
    form_class = GalleryForm
    title = _("Create Gallery")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

class EditGallery(GalleryMixin, OwnerUpdateMixin, UpdateView):
    """An owner of a gallery can edit it"""
    title = _("Edit Gallery")
    form_class = GalleryForm
    get_group = lambda self: None

class GalleryList(GalleryMixin, OwnerViewMixin, ListView):
    """List all galleries"""

class GalleryTally(GalleryMixin, DetailView):
    """Perform an STV ranking on the gallery votes"""
    template_name = 'resources/gallery_tally.html'

    def get_title(self):
        if self.object.contest_votes == 1:
            return _("Voting Results (STV)")
        return _("Voting Results (Count)")

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        gallery = data['object']
        if gallery.contest_finish > now().date() and not self.can_modify_gallery():
            raise PermissionDenied("Can not access this yet!")
        data['count'] = int(self.request.GET.get('count', 1))
        if gallery.contest_votes == 0:
            data['result'] = gallery_count(gallery, data['count'])
        else:
            data['result'] = gallery_stv(gallery, data['count'])
        return data

class GalleryTallyJson(GalleryMixin, DetailView):
    """Get the voting information as a json"""
    def get(self, request, gallery_id=None):
        gallery = self.get_object()
        if gallery.contest_finish > now().date() and not self.can_modify_gallery():
            raise PermissionDenied("Can not access this yet!")
        context = {}
        context['count'] = int(self.request.GET.get('count', 1))
        if gallery.contest_votes == 0:
            context['counts'] = []
            for item, count in gallery_count(gallery, context['count']):
                context['counts'].append({
                    'resource_id': item.pk,
                    'resource_name': item.name,
                    'votes': count})
        else:
            data = gallery_stv(gallery, context['count'])
            context['entries'] = [{'resource_id': value.pk, 'resource_name': value.name}
                for value in data['candidates']]
            context['stv_input'] = data['stv']
        return JsonResponse(context, safe=False)

class DeleteResource(OwnerDeleteMixin, DeleteView):
    """An owner of a resource can delete it"""
    model = Resource
    title = _("Delete")

class EditResource(OwnerUpdateMixin, UpdateView):
    """An owner of a resource can edit it"""
    model = Resource
    title = _("Edit")

    def get_form_class(self):
        return ResourceBaseForm.get_form_class(self.object)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        if self.object.gallery:
            kwargs['gallery'] = self.object.gallery
        return kwargs

    def get_success_url(self):
        return self.request.POST.get('next', self.object.get_absolute_url())


class CheckResource(SingleObjectMixin, RedirectView):
    """Contest checking, confirm resources are correct"""
    model = Resource

    def get_redirect_url(self, *args, **kwargs):
        return self.request.GET.get(
            'next', self.get_object().get_absolute_url())

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        gallery = obj.gallery

        if not request.user.is_authenticated and gallery.user != request.user and \
                (gallery.group not in request.user.groups.all()):
            raise PermissionDenied()

        if obj.checked_by:
            obj.checked_by = None
        else:
            obj.checked_by = request.user
        obj.save()
        return super(CheckResource, self).get(request, obj=obj)

class PublishResource(OwnerUpdateMixin, DetailView):
    """Any ownerof a resource can publish it"""
    model = Resource
    title = _("Publish")

    def get(self, request, *args, **kwargs):
        item = self.get_object()
        item.published = True
        item.save()
        messages.info(self.request, _('Resource now Published'))
        return redirect(item.get_absolute_url())

class RenameResource(OwnerUpdateMixin, UpdateView):
    """Rename the resource's download name"""
    template_name = 'resources/resource_rename.html'
    form_class = RenameResourceForm
    model = Resource

    def get_form_kwargs(self):
        # Allow url links to specify the new filename
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = {'new_name': self.request.GET.get('new_name')}
        return kwargs

class MoveResource(OwnerUpdateMixin, UpdateView):
    """Any owner of a resource and a gallery can move resources into them"""
    template_name = 'resources/resource_move.html'
    form_class = GalleryMoveForm
    model = Resource

    def get_title(self):
        if 'source' in self.kwargs:
            return _('Move to Gallery')
        return _('Copy to Gallery')

    def get_source(self):
        """Return the source gallery if needed"""
        if 'source' in self.kwargs:
            return get_object_or_404(Gallery, pk=self.kwargs['source'])
        return None

    def get_group(self):
        """This gives group members permisson to move other's resources"""
        return getattr(self.get_source(), 'group', None)

    def get_form_kwargs(self):
        kwargs = super(MoveResource, self).get_form_kwargs()
        kwargs['source'] = self.get_source()
        kwargs['user'] = self.request.user
        return kwargs

    def form_invalid(self, form):
        for error in form.errors.as_data().get('target', []):
            if error.code == 'invalid_choice':
                raise PermissionDenied()
        return super(MoveResource, self).form_invalid(form)

    def get_success_url(self):
        return self.get_object().get_absolute_url()


class UploadResource(OwnerCreateMixin, CreateView):
    """Any logged in user can create a resource"""
    form_class = ResourceForm
    model = Resource
    title = _("Upload New Resource")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        if hasattr(self, 'gallery'):
            kwargs['gallery'] = self.gallery
        return kwargs


class DropResource(UploadResource):
    """A drag and drop uploader using json"""
    template_name = 'resources/ajax/add.txt'
    content_type = 'text/plain'
    form_class = ResourceAddForm

    def form_valid(self, form):
        super().form_valid(form)
        context = self.get_context_data(item=form.instance)
        return self.render_to_response(context)

class UploadJson(UploadResource):
    """Upload with Json response"""
    form_class = ResourceAddForm

    def form_invalid(self, form):
        super().form_invalid(form)
        return JsonResponse(form.errors, status=400,
                            content_type='application/json; charset=utf-8')

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse(form.instance.as_json(), safe=False,
                            content_type='application/json; charset=utf-8')

class QuotaChecker(OwnerViewMixin, ListView):
    template_name = 'resources/quota_checker.html'
    title = _("Quota Checker")
    model = Resource

    def dispatch(self, request, *args, **kwargs):
        if request.user.username != kwargs['username'] and \
            not request.user.has_perm("resources.can_curate"):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['object_sorted'] = sorted(data['object_list'],\
            key=lambda item: item.disk_usage(), reverse=True)
        return data

class QuotaJson(View):
    """Returns a Json snippet with information about a user's quota"""
    def get(self, request):
        context = {'user': 'unknown', 'quota': 0, 'used': 0}
        if request.user.is_authenticated:
            context.update({
                'user': request.user.username,
                'used': request.user.resources.disk_usage(),
                'quota': request.user.quota(),
            })
        context['remain'] = context['quota'] - context['used']
        context['used_label'] = filesizeformat(context['used'])
        context['quota_label'] = filesizeformat(context['quota'])
        return JsonResponse(context, safe=False)

class BasicResourcesJson(View):
    """Returns information about resources"""
    def get(self, request):
        context = {}

        qset = Resource.objects.all()
        max_num = 1
        if 'pks[]' in request.GET:
            pks = request.GET.getlist('pks[]')
            qset = qset.filter(pk__in=pks)
            max_num += len(pks)
        elif 'q' in request.GET:
            query = Q()
            for qey in request.GET.getlist('q'):
                if qey and '://' in qey:
                    try:
                        qey = str(parse_any_url(qey, self.request.user).pk)
                    except (ValueError, AttributeError, KeyError) as err:
                        context['error'] = str(err)
                        qey = ''
                    except RemoteError as err:
                        context['error'] = "Remote server error! {}".format(str(err))
                        qey = ''

                max_num += 2
                if qey.isnumeric():
                    context['pk'] = qey
                    query |= Q(pk=qey)
                elif qey:
                    context['query'] = qey
                    query |= Q(name__iexact=qey)\
                           | Q(slug__iexact=qey)\
                           | Q(download__iendswith=qey)

            if not query:
                qset = qset.none()
            else:
                qset = qset.filter(query)
        else:
            qset = qset.none()

        if qset.count() > max_num:
            context['error'] = 'Too many results (>{})'.format(max_num)
            qset = qset.none()

        context['resources'] = [resource.as_json() for resource in qset]

        return JsonResponse(context, safe=False,
                            content_type='application/json; charset=utf-8')


class LinkToResource(UploadResource):
    """Create a link to a resource instead of an upload"""
    form_class = AddLinkForm
    title = _("Link to Video or Resource")

    def form_valid(self, form):
        """Redirect back to the edit page to finalise the situation"""
        super().form_valid(form)
        obj = form.save()
        url = reverse('edit_resource', kwargs={'pk': obj.pk})
        return HttpResponseRedirect(url)

class PasteInResource(UploadResource):
    """Create a paste-bin entry instead of an upload"""
    form_class = ResourcePasteForm
    title = _("New PasteBin")

    def get_context_data(self, **kw):
        data = super().get_context_data(**kw)
        data['object'] = Category.objects.get(slug='pastebin')
        data['object']._parent = self.request.user.resources.all()
        data['object_list'] = None
        return data

class ReleaseResource(UploadResource):
    """Uploading a release file"""
    form_class = ResourceReleaseForm
    title = _("New Release Upload")

class AddCodeResource(UploadResource):
    """Uploading code resource i.e. (extensions)"""
    template_name = 'resources/resource_code_form.html'
    form_class = ResourceCodeForm
    title = _("New Extension")

    def get_form_kwargs(self):
        user = self.request.user
        if not user.gpg_key:
            messages.error(self.request, _("You must have a GPG Key to upload extensions.")\
                + _(" <a href='%s'>Add One Now</a>.") % reverse('edit_profile'), extra_tags='safe')
            raise PermissionDenied("No GPG Key")
        kwargs = super().get_form_kwargs()
        kwargs['category'] = get_object_or_404(Category, slug='extension')
        return kwargs

class ViewResource(DetailView):
    """View a single resource, for download or just zommed in"""
    model = Resource

    def get_queryset(self):
        qset = Resource.objects.for_user(self.request.user)
        if 'username' in self.kwargs:
            qset = qset.filter(user__username=self.kwargs['username'])
        return qset

    def get_template_names(self):
        if self.request.GET.get('modal', False):
            return 'resources/resource_modal.html'
        return super().get_template_names()

    def get(self, request, *args, **kwargs):
        ret = super(ViewResource, self).get(request, *args, **kwargs)
        if self.object.is_new:
            if self.object.user == request.user:
                return redirect("edit_resource", self.object.pk)
            else:
                raise Http404()
        return ret

class TagsJson(View):
    """Json based get a list of possible tags that a resource can use"""
    def get(self, request):
        # We could leverage category to style
        # categorized tags differently in the suggestions list
        context = {"tags" : [{
            "id": tag.pk,
            "name": tag.name,
            "cat" : str(tag.category or "") or None,
        } for tag in Tag.objects.all()]}
        return JsonResponse(context, safe=False, content_type='application/json; charset=utf-8')


class VoteResource(SingleObjectMixin, OwnerCreateMixin, RedirectView):
    """Allow any logged in user to vote on a resource"""
    permanent = False
    queryset = Resource.objects.filter(published=True)
    msg = {
        'prev': _('Your vote rankings have been changed, thanks for voting!'),
        'done': _('Thank you for your vote!'),
        'undo': _('Previous Vote Removed!'),
        'ended': _('You may not vote after the contest ends.'),
        'disquo': _('You may not vote for disqualified entrires.'),
        '!begun': _('You may not vote until the contest begins.'),
        '!ready': _('You may not vote in a contest open for submissions.'),
        'myown': _('You can\'t vote on your own entry!'),
        'badgroup': _("You are not in the right team to vote."),
    }

    def get_redirect_url(self, *args, **kwargs):
        return self.request.GET.get(
            'next', self.get_object().get_absolute_url())

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        try:
            self.vote_on(obj, kwargs['like'] == '+')
        except PermissionDenied as err:
            messages.error(self.request, str(err))
        return super(VoteResource, self).get(request, obj=obj)

    def vote_on(self, item, like=True):
        """Vote on the item, say if the user likes it or not"""
        user = self.request.user
        gallery = item.gallery
        group = gallery.contest_group if gallery else None

        msg = self.msg['done'] if like else self.msg['undo']

        if group and not user.groups.filter(pk=group.pk):
            raise PermissionDenied(self.msg['badgroup'])

        if gallery and gallery.contest_submit:
            votes = self.contest_vote(item)

            # Remove previous votes if we aren't using ranked voting
            if gallery.contest_votes == 0:
                votes.delete()

            order = -1 # push item to front
            if not votes.filter(resource_id=item.pk):
                order = votes.count() # new item to the back

            self._basic_vote(item, like, order=order)

            # Re-order all the votes, -1 to 0, and all older votes after
            for x, vote in enumerate(votes.order_by('order')):
                if vote.resource == item and x != vote.order:
                    msg = self.msg['prev']
                vote.order = x
                vote.save()
        else:
            self._basic_vote(item, like)

        messages.info(self.request, msg)

    def _basic_vote(self, item, like=True, order=0):
        if item.user_id == self.request.user.pk:
            raise PermissionDenied(self.msg['myown'])
        if like:
            item.votes.update_or_create(voter_id=self.request.user.pk, defaults={'order':order})
        else:
            item.votes.filter(voter_id=self.request.user.pk).delete()
        # Update the Resource item's vote count (for easier lookup)
        item.votes.refresh()

    def contest_vote(self, item):
        """Attempt to vote on the item, but fail if the contest isn't running"""
        gallery = item.gallery
        today = now().date()
        # Some different rules for contest galleries
        if gallery.contest_submit > today:
            raise PermissionDenied(self.msg['!begun'])
        elif gallery.contest_voting and gallery.contest_voting > today:
            raise PermissionDenied(self.msg['!ready'])
        elif gallery.is_counting or gallery.is_finished:
            raise PermissionDenied(self.msg['ended'])
        elif gallery.contest_checks and not item.checked_by:
            raise PermissionDenied(self.msg['disquo'])
        return item.gallery.votes.filter(voter_id=self.request.user.pk)

class DownloadReadme(ViewResource):
    """The readme.txt file is generated from the resource's description"""
    template_name = 'resources/readme.txt'
    content_type = "text/plain"

class DownloadResource(ViewResource):
    """Resource download will count the downloads and then redirect users"""
    template_name = 'resources/view_text.html'

    def get(self, request, *args, **kwargs):
        func = kwargs.get('fn', None)
        item = self.get_object()
        if not item.download:
            messages.warning(request, _('There is no file to download in this resource.'))
            return redirect(item.get_absolute_url())

        # The view 'download' allows one to view an image in full screen glory
        # which is technically a download, but we count it as a view and try
        # and let the browser deal with showing the file.
        if func is None:
            if item.mime().is_text():
                return super(DownloadResource, self).get(request, *args, **kwargs)
            item.fullview += 1
            item.save(update_fields=('fullview',))
            return redirect(item.download.url)

        # Otherwise the user intends to download the file and we record it as
        # such before passing the download path to nginx for delivery using a
        # content despatch to force the browser into saving-as.
        item.downed += 1
        item.save(update_fields=('downed',))

        if func not in ['download', item.filename()]:
            messages.warning(request, _('Can not find file \'%s\', please retry download.') % func)
            return redirect(item.get_absolute_url())

        # But live now uses nginx directly to set the Content-Disposition
        # since the header will be passed to the fastly cache from nginx
        # but the sendfile method will fail because of the internal redirect.
        return redirect(item.download.url.replace('/media/', '/dl/'))

class UnpublishedGallery(ListView):
    model = Resource
    paginate_by = 20
    title = _('My Unpublished Resources')

    def get_queryset(self):
        qset = super().get_queryset()
        if self.request.user.is_authenticated:
            qset = qset.filter(published=False, is_removed=False, user=self.request.user)
        else:
            qset = qset.none()
        return qset.order_by('-edited')

class FavoriteResources(ListView):
    model = Resource
    paginate_by = 20
    title = _('Favorites')

    def get_queryset(self):
        user = self.request.user
        if 'username' in self.kwargs:
            user = get_object_or_404(User, username=self.kwargs['username'])
        return user.favorites.items()

class ResourceList(CategoryListView):
    """
    This listing of resources provides the backbone of all gallery and resource
    listings on the website. It provies searching as well as being used by the
    RSS feed generator.
    """
    rss_view = 'resources_rss'
    parade_view = 'resources_parade'
    model = Resource
    opts = ( # type: ignore
        ('username', 'user__username'),
        ('team', 'galleries__group__team__slug', False),
        ('gallery_id', 'galleries__id', False),
        ('tags', 'tags__name__in', False),
    )
    cats = ( # type: ignore
        #('media_type', _("Media Type")),
        ('category', _("Media Category"), 'get_categories'),
        ('license', _("License"), 'get_licenses'),
        ('galleries', _("Galleries"), 'get_galleries'),
    )
    order = '-liked' # type: ignore
    orders = ( # type: ignore
        ('-liked', _('Most Popular')),
        ('-viewed', _('Most Views')),
        ('-downed', _('Most Downloaded')),
        ('-edited', _('Last Updated')),
        ('extra_status', None),
    )

    def base_queryset(self):
        qset = super(ResourceList, self).base_queryset()
        if not self.request.user.has_perm('moderation.can_moderate'):
            qset = qset.exclude(is_removed=True)
        return qset

    def get_template_names(self):
        if self.get_value('category'):
            return ['resources/resourcegallery_specific.html']
        return ['resources/resourcegallery_general.html']

    def extra_filters(self):
        if not self.is_user and not self.in_team:
            return dict(published=True)
        return {}

    @property
    def is_user(self):
        """Returns True if the user is defined in the request arguments"""
        if not hasattr(self.request, 'my_is_user'):
            username = self.request.user.username
            self.request.my_is_user = self.get_value('username') == username
        return self.request.my_is_user

    @property
    def in_team(self):
        """Returns True if the user is in a team"""
        if not hasattr(self.request, 'my_in_team'):
            if self.request.user.is_authenticated:
                teams = self.request.user.teams
            else:
                teams = Team.objects.none()
            slug = self.get_value('team')
            self.request.my_in_team = teams.filter(slug=slug).count() == 1
        return self.request.my_in_team

    def get_licenses(self):
        """Return a list of licenses"""
        return License.objects.filter(filterable=True)

    def get_categories(self):
        """Return a list of Categories"""
        return Category.objects.all()

    def get_galleries(self):
        """Return a list of galleries depending on the user/team selection"""
        if 'username' in self.kwargs:
            user = get_object_or_404(User, username=self.kwargs['username'])
            return user.galleries.exclude(status="=")
        team = self.get_value('team')
        if team:
            return get_object_or_404(Group, team__slug=team).galleries.all()
        category = self.get_value('category')
        if category:
            return Gallery.objects.filter(category__slug=category)
        return None

    def get_context_data(self, **kwargs):
        """Add all the meta data together for the template"""
        data = super(ResourceList, self).get_context_data(**kwargs)
        for key in self.kwargs:
            if data.get(key, None) is None:
                raise Http404("Item %s not found" % key)

        if 'team' in data and data['team']:
            # Our options are not yet returning the correct item
            data['team'] = get_object_or_404(Group, team__slug=data['team'])
            data['team_member'] = self.in_team
            data['object_list'].instance = data['team']
        elif 'username' in self.kwargs:
            data['is_user'] = self.is_user
            if 'username' not in data or not data['username']:
                raise Http404("User not found")
            data['object_list'].instance = data['username']

        if 'galleries' in data:
            # our options are not yet returning the correct item
            data['galleries'] = get_object_or_404(Gallery, slug=data['galleries'])
            data['object'] = data['galleries']

        if not isinstance(data.get('category', None), Category):
            data.pop('category', None)

        if 'category' in data and 'object_list' in data:
            data['tag_categories'] = data['category'].tags.all()
            if not ('galleries' in data and getattr(data['galleries'], 'category', None) \
                      and not data['username'] and not data['team']):
                if 'object' in data:
                    # Set parent manually, since categories don't naturally have parents.
                    data['category']._parent = data['object']
                else:
                    data['category']._parent = data['object_list']
                data['object'] = data['category']

            # Remove media type side bar if category isn't filterable.
            if not data['category'].filterable:
                for cat in data['categories']:
                    if cat is not None and cat.cid == 'category':
                        cat[:] = [cat.value]

        if 'tags' in data:
            data['tag_clear_url'] = self.get_url(exclude='tags')

        if self.is_user or ('galleries' in data and self.in_team):
            k = {}
            if data.get('galleries', None) is not None:
                k['gallery_id'] = data['galleries'].pk
            data['upload_url'] = reverse("resource.upload", kwargs=k)
            data['upload_drop'] = reverse("resource.drop", kwargs=k)

        data['limit'] = self.paginate_by
        if hasattr(data.get('object_list', None), '_clone'):
            # Only apply for actual django querysets
            data['full_object_list'] = data['object_list']._clone()
            data['full_object_list'].query.clear_limits()
        return data

class ResourceParade(ResourceList):
    paginate_by = 200
    def get_template_names(self):
        return ['resources/resourcegallery_parade.html']

class GalleryView(ResourceList):
    """Allow for a special version of the resource display for galleries"""
    opts = ResourceList.opts + (('galleries', 'galleries__slug', False),) # type: ignore
    cats = (('category', _("Media Category"), 'get_categories'),) # type: ignore

    def get_gallery(self):
        """Get the Gallery in context object"""
        if not hasattr(self, '_gallery'):
            opts = dict(self.get_value_opts)
            self._gallery = get_object_or_404(Gallery, slug=opts['galleries'])
        return self._gallery

    def get_template_names(self):
        return ['resources/resourcegallery_specific.html']

    def base_queryset(self):
        qset = super().base_queryset()
        gallery = self.get_gallery()
        if not self.query and gallery.is_contest:
            # Add a rank based on the order of voting.
            votes = Vote.objects.filter(resource=OuterRef('pk'), voter_id=self.request.user.pk)
            qset = qset.annotate(rank=Subquery(votes.values('order')[:1]))
        return qset

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        gallery = self.get_gallery()
        if gallery.is_contest and self.request.user.is_authenticated:
            groups = self.request.user.groups
            group = gallery.contest_group
            data['user_can_vote'] = gallery.is_voting and (not group or groups.filter(pk=group.pk))
        return data

    @property
    def order(self):
        """Returns the ordering in this gallery view (the first one only)"""
        return self.orders[0][0]

    @property
    def orders(self):
        """Restrict ordering when doing a contest"""
        gallery = self.get_gallery()
        if gallery.is_contest:
            if gallery.is_submitting:
                return (('-created', _('Created Date')),)
            elif gallery.is_voting or gallery.is_counting:
                return (('rank,?', _('Random Order')),)
            return (
                ('-liked', _('Most Votes')),
                ('-created', _('Created Date')),
            )
        return super(GalleryView, self).orders

    @property
    def paginate_by(self):
        """Turn off pagination when voting"""
        gallery = self.get_gallery()
        if gallery.is_contest and gallery.is_voting:
            return 0
        return 20

class GalleryParade(GalleryView):
    paginate_by = 200
    def get_template_names(self):
        return ['resources/resourcegallery_parade.html']

class ResourcePick(ResourceList):
    """A loadable picker that allows an item to be choosen"""
    def get_template_names(self):
        return ['resources/resource_picker.html']

class ResourceFeed(ListFeed):
    """A list of resources in an RSS Feed"""
    list_class = ResourceList

    @property
    def title(self):
        context = self.list.context_data
        if 'object' in context:
            return str(context['object'])
        return _("Resources Feed")

    @property
    def description(self):
        context = self.list.context_data
        if 'object' in context:
            if hasattr(context['object'], 'desc'):
                return context['object'].desc
            elif hasattr(context['object'], 'description'):
                return context['object'].description
        return "Resources RSS Feed"

class GalleryFeed(ListFeed):
    list_class = GalleryView
    image_size = '190'

    @property
    def gallery(self):
        if not hasattr(self, '_gallery'):
            self._gallery = get_object_or_404(Gallery, slug=self.kwargs['galleries'])
        return self._gallery

    link = lambda self: self.gallery.get_absolute_url()
    title = lambda self: self.gallery.name
    feed_url = lambda self: self.link() + 'rss/'
    image_url = lambda self: self.media_url(self.gallery.thumbnail_url())
    description = lambda self: self.gallery.desc or _('Gallery Resources RSS Feed')
    author_name = lambda self: self.gallery.group

class ResourceJson(ViewResource, ExternalMixin):
    def render_to_response(self, context, **_):
        return JsonResponse(context, encoder=ResourceJSONEncoder)

    def user_or_none(self, user):
        """Return a link to the user, or None"""
        if user is not None:
            return {
                'name': str(user),
                'link': self.site_url(user.get_absolute_url()),
                'username': user.username,
            }
        return None

    def _get_revision(self, child):
        ret = {
            'version': child.version,
            'created': child.created.isoformat(),
            'downloaded': child.downed,
            'file': self.media_url(child.download),
            'signature': self.media_url(child.signature),
            'verified': child.verified,
            'checked_by': self.user_or_none(child.checked_by),
            'checked_sig': self.media_url(child.checked_sig),
        }
        ret.update(child.tags.as_dict())
        return ret

    def get_context_data(self, **kwargs):
        item = self.get_object()
        ret = {
            'id': item.external_id,
            'name': item.name,
            'desc': item.desc,
            'website': item.link,
            'type': item.media_type,
            'category': item.category.slug,
            'author': self.user_or_none(item.user),
            'html': self.site_url(item.get_pk_url()),
            #'file': self.media_url(item.download),
            'preview': self.media_url(item.rendering),
            'liked': item.liked,
            # All revisions, plus self as a standard format
            'revisions': \
                [self._get_revision(child) for child in item.revisions.all()] + \
                [self._get_revision(item)],
            'comments': [{
                'author': comment.name,
                'text': comment.comment,
                'flags': comment.flag_count,
            } for comment in item.comments.annotate(
                flag_count=Count('flags', filter=Q(flag='\U0001F44D'))).order_by('-flag_count')],
        }
        return ret

class ResourcesJson(ResourceList, ExternalMixin):
    """Take any list of resources, and produce json output"""
    def render_to_response(self, context, **_):
        return JsonResponse(context, encoder=ResourceJSONEncoder)

    def get_context_data(self, object_list, **kwargs):
        # Xapian does not filter these items correctly, so we do this for non-queries.
        if not self.query and self.request.GET.get('checked', False):
            object_list = object_list.filter(checked_by_id__isnull=False, verified=True)
        if self.request.GET.get('limit', None):
            object_list = object_list[:int(self.request.GET['limit'])]
        return {
            'query': self.query,
            'items': list(self.layout_items(object_list)),
        }

    def layout_items(self, lst):
        checked = bool(self.request.GET.get('checked', False))
        for item in lst:
            if item is None or not item.download:
                continue # Don't accept plain links
            if checked and (not item.checked_by_id or not item.verified):
                continue # Ignore unverified items
            ret = {
                'id': item.external_id,
                'author': str(item.user),
                'name': item.name,
                'type': item.media_type,
                'license': item.license and item.license.code or '',
                'icon': self.media_url(item.thumbnail),
                'summary': (item.desc or '').strip().split("\n", 1)[0],
                'links': {
                    'html': self.site_url(item.get_pk_url()),
                    'json': self.site_url(item.get_pk_url('.json')),
                    'file': self.media_url(item.download),
                },
                'verified': item.verified,
                'stats': {
                    'liked': item.liked,
                    'downloaded': item.downed,
                    'revisions': item.revisions.count(),
                    'extra': item.extra_status or 0,
                },
                'dates': {
                    'created': item.created.isoformat(),
                    'edited': item.edited.isoformat(),
                },
            }
            ret.update(item.tags.as_dict())
            ret['previous'] = item.old_tags.as_dict()
            yield ret

class GalleryJson(GalleryView):
    """Special version of resourcejson but for galleries"""
    def render_to_response(self, context, **_):
        return JsonResponse(context, encoder=ResourceJSONEncoder)
