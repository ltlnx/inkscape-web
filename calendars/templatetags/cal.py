#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Show the datetime or a relative time field.
"""

from datetime import datetime

# Not the typical translator
from django.utils.translation import ungettext_lazy as _un, ugettext_lazy as _
from django.utils.timezone import is_aware, utc
from django.template import Library
from django.forms import widgets

register = Library() # pylint: disable=invalid-name

TEMPLATE_AGO = _('%s ago')
TEMPLATE_UNTIL = _('in %s')
TEMPLATE_SEP = _('%s and %s')
TEMPLATE_FOR = _('Right now! For %s')
CHUNKS = (
    # Note to translator: Short varients are for display. Keep short but use your better judgement.
    (60 * 60 * 24 * 365, _un('%d year', '%d years'), _('%dyr')),
    (60 * 60 * 24 * 30, _un('%d month', '%d months'), _('%dmon')),
    (60 * 60 * 24 * 7, _un('%d week', '%d weeks'), _('%dwk')),
    (60 * 60 * 24, _un('%d day', '%d days'), _('%dd')),
    (60 * 60, _un('%d hour', '%d hours'), _('%dhr')),
    (60, _un('%d minute', '%d minutes'), _('%dmin'))
)

@register.filter("relative")
def relative_time(dt_start, dt_end):
    """
    Turn the datetime into a relative datetime signature
    """
    if not dt_start:
        return _('Never')

    now = datetime.now(utc if is_aware(dt_start) else None)

    # Convert datetime.date to datetime for comparison.
    if not isinstance(dt_start, datetime):
        dt_start = datetime(dt_start.year, dt_start.month, dt_start.day, tzinfo=dt_start.tzinfo)

    if dt_end:
        if not isinstance(dt_end, datetime):
            dt_end = datetime(dt_end.year, dt_end.month, dt_end.day, tzinfo=dt_end.tzinfo)
        # Special output for DURING event
        if dt_start < now < dt_end:
            return make_ago(dt_end - now, True, TEMPLATE_FOR)
        # Make 'time since' relative to the END of the event
        if now > dt_end:
            dt_start = dt_end
    return make_ago(now - dt_start, now < dt_start, TEMPLATE_UNTIL, TEMPLATE_AGO)

def make_ago(delta, past=None, t_until=TEMPLATE_UNTIL, t_ago=TEMPLATE_AGO):
    delta_seconds = delta.days * 24 * 60 * 60 + delta.seconds
    # Detect past or future based on sign
    if past is None:
        past = (delta_seconds > 0)
    template = t_until if past else t_ago
    delta_seconds = abs(delta_seconds)
    for i, (seconds, name, small) in enumerate(CHUNKS):
        count = delta_seconds // seconds
        # If there's a positive number in this unit, OR we have the last unit.
        if count != 0 or i == len(CHUNKS) - 1:
            result = name % count
            if i + 1 < len(CHUNKS):
                # Now get the second item unit if in mode 0
                seconds2, name2 = CHUNKS[i + 1][:2]
                count2 = (delta_seconds - (seconds * count)) // seconds2
                if count2 != 0:
                    result = TEMPLATE_SEP % (result, (name2 % count2))
            return template % result
    return '?'

@register.filter()
def is_member(user, event):
    """Is the user subscribed to this gallery"""
    return event.is_member(user)

