#
# Copyright 2013-2018, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#

from django.contrib.admin import ModelAdmin, site, TabularInline

from .models import Event, EventException, EventOccurance, EventAgendum
from .forms import EventForm, ExceptionForm

class ExceptionInline(TabularInline):
    model = EventException
    form = ExceptionForm
    extra = 1

class TeamEventAdmin(ModelAdmin):
    list_display = ('title', 'team', 'start', 'end', 'creator')
    search_fields = ('title', 'description')
    list_filter = ('team', 'start')
    raw_id_fields = ('creator',)
    inlines = [ExceptionInline]
    form = EventForm

site.register(Event, TeamEventAdmin)

class AgendumInline(TabularInline):
    model = EventAgendum
    extra = 1
    raw_id_fields = ('creator', 'completor', 'deferred_to')

class EventOccuranceAdmin(ModelAdmin):
    raw_id_fields = ('event', 'presided',)
    list_display = ('event', 'start',)
    search_fields = ('event__title', 'event__description')
    list_filter = ('event', 'start')
    inlines = [AgendumInline]

site.register(EventOccurance, EventOccuranceAdmin)
