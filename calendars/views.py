# -*- coding: utf-8 -*-
#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Calendars for teams
"""

from django.http import Http404
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView
from django.views.generic.base import RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.core.exceptions import PermissionDenied

from django_ical.utils import build_rrule_from_recurrences_rrule
from django_ical.views import ICalFeed

from person.models import Team
from person.mixins import TeamMixin

from .patch import feedgenerator
from .models import Event, EventOccurance, EventAgendum

class TeamEvent(DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        dtm = now()
        obj = self.get_object();
        data['prev_occurance'] = obj.prev_occurance(dtm)
        data['occurance'] = obj.current_occurance()

        # If nothing is happening right now (strictly) look for firstly past
        # events which are not closed yet. This keeps past meetings in scope
        if data['occurance'] is None:
            oce = obj.occurances.filter(is_started=True , is_finished=False).last()
            if oce:
                data['occurance'] = oce.get_oc()
        # Sendingly the next occurance for editing the future agenda.
        if data['occurance'] is None:
            data['occurance'] = obj.next_occurance(dtm)

        # Sometimes we are looking at the previous already
        if data['occurance'] == data['prev_occurance']:
            data['prev_occurance'] = None

        data['now'] = now()
        return data

class OccuranceMixin:
    def get_queryset(self):
        return self.model.objects.filter(event_id=self.kwargs['event_id'])

class TeamEventOccurance(OccuranceMixin, DetailView):
    model = EventOccurance

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['now'] = now()
        data['occurance'] = self.get_object().get_oc()
        data['title_only'] = True
        data['title'] = f"{data['object'].event} - {data['object']}"
        return data

class MeetingControl(OccuranceMixin, SingleObjectMixin, RedirectView):
    """Set the meeting to certain states as it's being run"""
    model = EventOccurance
    permanent = False

    def get_redirect_url(self, event_id, pk, action):
        self.note = self.request.POST.get('note', None)
        obj = self.get_object()
        if not obj.event.is_member(self.request.user):
            raise PermissionDenied("Not a memeber of the calendar's team")
        try:
            getattr(self, f"do_{action}")(obj)
            obj.save()
        except AttributeError:
            raise Http404("Bad action name")
        return obj.event.get_absolute_url()

    def do_start(self, obj):
        """Start the meeting"""
        if obj.presided:
            raise PermissionDenied("Already started")
        obj.presided = self.request.user
        obj.is_started = True

    def do_pause(self, obj):
        """Pause the meeting"""
        if obj.presided != self.request.user:
            raise PermissionDenied("The meeting president must pause the meeting")
        obj.presided = None

    def do_finish(self, obj):
        if obj.presided != self.request.user:
            raise PermissionDenied("The meeting president must finish the meeting")
        obj.is_finished = True
        obj.minutes = self.note

    def do_agendum(self, obj):
        if not self.note:
            raise PermissionDenied("Can't create an empty agendum")
        obj.agenda.create(creator=self.request.user, detail=self.note)

class EditAgendum(SingleObjectMixin, RedirectView):
    """Edit and delete individual agenda items"""
    model = EventAgendum
    permanent = False

    def get_queryset(self):
        return self.model.objects.filter(
            eoc__event_id=self.kwargs['event_id'],
            eoc_id=self.kwargs['eoc'])

    def get_redirect_url(self, event_id, eoc, pk):
        obj = self.get_object()
        if self.request.user not in (obj.creator, obj.eoc.presided):
            raise Http404("Not the owner of this agendum")
        if obj.eoc.is_finished:
            raise Http404("Can not modify completed agenda")

        url = obj.eoc.event.get_absolute_url()
        self.action(obj)
        return url

    def action(self, obj):
        note = self.request.POST.get('note', None)
        if note:
            obj.detail = note
            obj.save()
        else:
            obj.delete()

class DeferAgendum(EditAgendum):
    def action(self, obj):
        noc = obj.eoc.event.next_occurance()
        if not noc:
            raise Http404("No next occurance!")
        next_eoc = noc.details
        next_obj = next_eoc.agenda.create(
            creator=obj.creator,
            detail=obj.detail,
            link=obj.link,
        )
        obj.deferred_to = next_obj
        obj.save()

class CompleteAgendum(EditAgendum):
    def action(self, obj):
        obj.completed = now()
        # We may want to give the user a way of saying who at the meeting completed this
        obj.completor = self.request.user
        obj.save()


class TeamEventList(TeamMixin, ListView):
    queryset = Team.objects.exclude(enrole='S')

    def get_object(self):
        return None

    def get_parent(self):
        try:
            return self.queryset.get(slug=self.kwargs['team'])
        except Team.DoesNotExist:
            raise Http404("Team doesn't exist!")

    def get_queryset(self):
        return self.get_parent().events.all()

    def get_context_data(self):
        data = super().get_context_data()
        data['team'] = self.get_parent()
        data['now'] = now()
        return data

class EventList(ListView):
    title = _("Full Project Calendar")
    model = Event

    def get_context_data(self):
        data = super().get_context_data()
        data['now'] = now()
        return data

class TeamEventFeed(TeamMixin, ICalFeed):
    """An ics event feed"""
    queryset = Team.objects.exclude(enrole='S')
    product_id = '-//inkscape.org//Calendar//EN'
    timezone = 'UTC'

    def get_object(self, request, team=None):
        self.request = request
        self.object = None
        if team is not None:
            try:
                self.object = self.queryset.get(slug=team)
            except Team.DoesNotExist:
                raise Http404("No team of that name.")
        return self.object

    def file_name(self, obj):
        if obj is not None:
            return f"{obj.slug}_cal.ics"
        return f"inkscape_cal.ics"

    def items(self):
        if self.object:
            return self.object.events.order_by('-start')
        return Event.objects.order_by('-start')

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        from django.contrib.sites.models import Site
        domain = Site.objects.get().domain
        url = item.get_absolute_url().lstrip('/')
        if '://' not in url:
            url = f"https://{domain}/{url}"
        return url

    def item_start_datetime(self, item):
        return item.start_tz

    def item_end_datetime(self, item):
        return item.end_tz

    def item_rrule(self, item):
        """Adapt Event recurrence to Feed Entry rrule."""
        if item.recurrences:
            rules = []
            for rule in item.recurrences.rrules:
                rules.append(build_rrule_from_recurrences_rrule(rule))
            return rules

    def item_exrule(self, item):
        """Adapt Event recurrence to Feed Entry exrule."""
        if item.recurrences:
            rules = []
            for rule in item.recurrences.exrules:
                rules.append(build_rrule_from_recurrences_rrule(rule))
            return rules

    def item_rdate(self, item):
        """Adapt Event recurrence to Feed Entry rdate."""
        for exception in item.exceptions.filter(new_start__isnull=False):
            yield item.localize(exception.new_start)

    def item_exdate(self, item):
        """Adapt Event recurrence to Feed Entry exdate."""
        for exception in item.exceptions.filter(old_start__isnull=False):
            yield item.localize(exception.old_start)

    def item_alarm(self, item):
        """Attempt to set an hour based VALARM"""
        try:
            return int(self.request.GET.get('alarm', 0))
        except (ValueError, TypeError):
            return None
